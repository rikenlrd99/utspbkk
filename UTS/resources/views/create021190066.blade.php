<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Biodata</title>
</head>
<style>
*{
  margin: 0 auto;
}

body{
  width: 70%;
  min-width: 500;
  max-width: 1270;
}

input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>
<body>
    <h2> Input Biodata </h2>
    <div>
  <form action="{{ url('/proses') }}" method="POST">
      @csrf
      
    <label for="npm">NPM</label>
    <input type="text" name="npm" >

    <label for="nama">Nama</label>
    <input type="text" name="nama">

    <label for="program">Program Studi</label>
    <input type="text" name="program" >

    <label for="nomor">No HP</label>
    <input type="text" name="nomor">

    <label for="ttl">Tempat Tanggal Lahir</label>
    <input type="text" name="tanggal">

    <label for="jk">Jenis Kelamin</label>
    <input type="text" name="jenis">

    <label for="agama">Agama</label>
    <input type="text" name="agama" >
  
    <input type="submit" value="Submit">
  </form>
</div>

</body>
</html>