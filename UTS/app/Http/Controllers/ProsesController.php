<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controller
{
    function create(){
        return view('create021190066');
    }

    function proses(Request $request){
        $data = array();
        $data['npm'] = $request->npm;
        $data['nama'] = $request->nama;
        $data['program'] = $request->program;
        $data['nomor'] = $request->nomor;
        $data['tanggal'] = $request->tanggal;
        $data['jenis'] = $request->jenis;
        $data['agama'] = $request->agama;

        return view('view021190066', ['data' => $data]);
    }
}
